package uinbdg.fisip.inputnilai.data

import android.view.View
import uinbdg.fisip.inputnilai.R

/*
    This is data models for placing menus in application.
 */
data class HomeMenu(val title: String = "", val desc: String = "", val icon: Int = -1, val color: Int = R.color.black, val onClickListener: View.OnClickListener = View.OnClickListener{})
data class MoreMenu(val title: String = "", val desc: String = "", val icon: Int = -1, val color: Int = R.color.black, val onClickListener: View.OnClickListener = View.OnClickListener{})