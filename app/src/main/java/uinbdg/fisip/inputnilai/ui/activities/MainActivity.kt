package uinbdg.fisip.inputnilai.ui.activities

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import uinbdg.fisip.inputnilai.R
import uinbdg.fisip.inputnilai.data.HomeMenu
import uinbdg.fisip.inputnilai.ui.activities.base.BaseLoggedActivity
import uinbdg.fisip.inputnilai.ui.adapters.MainMenuAdapter
import uinbdg.fisip.inputnilai.ui.layoutmanager.HomeMenuLayoutManager

class MainActivity : BaseLoggedActivity() {

    private val menus: ArrayList<HomeMenu> = ArrayList()

    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MainMenuAdapter(menus)

        main_menu_rv.layoutManager      = HomeMenuLayoutManager(this, 4)
        main_menu_rv.adapter            = adapter

        addMenus()
    }

    private fun addMenus() {
        menus.add(HomeMenu("Ujian Penelitian", "", R.drawable.ic_dashboard, R.color.green_500, View.OnClickListener {  }))
        menus.add(HomeMenu("Kerja Praktik", "", R.drawable.ic_industry, R.color.black, View.OnClickListener {  }))
        menus.add(HomeMenu("Ujian Komprehensif", "", R.drawable.ic_catalog, R.color.red_500, View.OnClickListener {  }))
        menus.add(HomeMenu("Lebih Banyak", "", R.drawable.ic_apps, R.color.blue_500, View.OnClickListener{ launchActivity<MoreMenuActivity>() }))
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan sekali lagi untuk keluar.", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}
